#!/bin/bash
# Stop on any errors
set -e
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
NETBRICKS_DIR="$HOME/rust-projects/NetBricks"
NATIVE_LIB_PATH="${NETBRICKS_DIR}/native"
cmd=$1
shift
executable=${BASE_DIR}/target/release/$cmd
if [ ! -e ${executable} ]; then
  echo "${executable} not found..searching for debug"
  executable=${BASE_DIR}/target/release/$cmd
  if [ ! -e ${executable} ]; then
     echo "${executable} not found"
  fi
fi
export LD_LIBRARY_PATH="${NATIVE_LIB_PATH}"
sudo env PATH="$PATH" LD_LIBRARY_PATH="$LD_LIBRARY_PATH" LD_PRELOAD="$LD_PRELOAD" \
  $executable -p 01:00.0 -m 0 -c 2 -n act
#  $executable -p 04:00.0 -p 03:00.1 -m 0 -c 2 -c 3 -n testing123
#  $executable -p 04:00.0 --master=0 --core=3
#  $executable "$@
