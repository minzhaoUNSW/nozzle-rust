extern crate rand;
extern crate dns_parser;
use dns_parser::Packet as Dns_Packet;
use dns_parser::RRData as RrData;
use dns_parser::Header as Dns_Header;
use e2d2::headers::*;
use e2d2::operators::*;
use e2d2::scheduler::*;
use e2d2::utils::Flow;
use std::sync::mpsc::Sender;
use self::rand::Rng;

#[derive(Debug, Clone)]
pub struct HostFlowMsg {
   pub host_ip : u32,
   pub flow : Flow,
   pub len : u32
}

#[derive(Debug, Clone)]
pub struct FlowStateMsg {
   pub flow : Flow,
   pub len : u32,
   pub pushed : bool
}

#[derive(Debug,Clone)]
pub struct DnsMsg {
    pub host_ip : u32,
    pub header : Dns_Header,
    pub question_name : String,
    pub answer_name : String,
    pub answer_ttl : u32,
   // pub answer_data : RrData,

}

const R_THREAD: u32 =  16;
const DNS_THREAD: u32 = 1;
/*tx: &Vec<Sender<FlowStateMsg>>, host_tx: &Vec<Sender<HostFlowMsg>>,, dns_tx: &Vec<Sender<DnsMsg>>*/
pub fn ttl_nf<T: 'static + Batch<Header = NullHeader>, S: Scheduler + Sized>(
    parent: T, _s: &mut S, host_tx: &Vec<Sender<HostFlowMsg>>, dns_tx: &Vec<Sender<DnsMsg>>) -> CompositionBatch {
    println!("Number of ttl_nf");
   // let tx_clone = tx.clone();
    //let dns_flag: bool = false;
    let dns_tx_clone = dns_tx.clone();
    let host_tx_clone = host_tx.clone();

    let mut groups = parent
        .parse::<MacHeader>()
        .parse::<IpHeader>()
        .group_by(2, box move |p| if {
            match p.get_header().flow() {
                Some(flow) => {

                    //host level operation
                    let host_msg = HostFlowMsg {
                        host_ip: flow.dst_ip, //using src ip since the bug for ip resolving?
                        flow:flow,
                        len : p.get_header().length() as u32
                    };

                    let host_msg_clone = host_msg.clone();
//call thread to send host data structure
                    let host_thread_index: u32 = host_msg.host_ip & (R_THREAD-1);
                    if let Result::Err(err) = host_tx_clone[host_thread_index as usize].send(host_msg){
                        println!("Error sending from pipeline for host: {}",err)
                    };

                    //println!("input host msg ip {}",host_msg_clone.host_ip);

                    if ((flow.dst_port == 53)||(flow.src_port == 53)) && flow.proto == 17 {
                        true
                    } else {false}
                },
                None => (false)
            }
            } {0} else {1}, _s,);

    let pipeline = groups
        .get_group(1)
        .unwrap()
        .map(box move |pkt| {
         /*   match pkt.get_header().flow() {
                Some(flow) => {
                    let flow_corrected = Flow {
                        src_ip: flow.src_ip,
                        dst_ip: flow.dst_ip,
                        src_port: flow.dst_port,
                        dst_port: flow.src_port,
                        proto: flow.proto,
                    };

                    let msg = FlowStateMsg {
                      flow: flow_corrected,
                      len: pkt.get_header().length() as u32,
                      pushed: false
                    };

                     let thread_index: u32  = msg.flow.src_ip & (R_THREAD-1);
                     if let Result::Err(err) = tx_clone[thread_index as usize].send(msg) {
                      println!("Error sending from pipeline: {}",  err);
                    }

                },
                None => ()
            };*/

        }).compose();

    let dns_pipeline = groups
        .get_group(0)
        .unwrap()
        .metadata(box move |pkt|{
            let flow = pkt.get_header().flow().unwrap();
            flow
        })
        .parse::<UdpHeader>()
        .map(box move |pkt| {
            let flow = pkt.read_metadata();
            //println!("FLOW:::{:?}",flow);
            match Dns_Packet::parse(pkt.get_payload()) {
                Ok(dns_pkt) => {
                    //println!("DNS Packet received in pipeline");
                    //println!("DNS:{:?}",dns_pkt);
               //     let mut dns_msg = DnsMsg {
                 //       data : Vec::new()
                   // };
                    //println!("{:?}",dns_pkt.header);
                    let mut dns_msg = DnsMsg{
                        host_ip : flow.dst_ip,
                        header : dns_pkt.header,
                        question_name : String::new(),
                        answer_name : String::new(),
                        //answer_data : RRdata,
                        answer_ttl : 0
                    };

                  for y in &dns_pkt.answers{
                    //println!("{:?}",y);

                    dns_msg.question_name = String::new();
                    dns_msg.answer_name = String::new();
                    dns_msg.answer_ttl = 0;

                    for x in &dns_pkt.questions{
                        dns_msg.question_name.push_str(&x.qname.to_string());
                        //println!("{:?}",dns_msg);
                        //dns_msg.question_name.push(' ');
                    }

                    let dns_thread_index: u32  = DNS_THREAD-1;

              
                      //let y_clone = y.clone();
                      dns_msg.answer_name.push_str(&y.name.to_string());
                      dns_msg.answer_ttl = y.ttl;
                     // dns_msg.answer_data = y.data;

                      let dns_msg_clone = dns_msg.clone();
                    
                      if let Result::Err(err) = dns_tx_clone[dns_thread_index as usize].send(dns_msg_clone) {
                          println!("Error sending from pipeline: {}",  err);
                      }

                    }
                },
                Err(error) => {
                    //println!("DNS Parsing failed {}", error);
                }
            };
        }).compose();

     merge(vec![pipeline, dns_pipeline]).compose()
}
