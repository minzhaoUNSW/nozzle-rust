extern crate time;
use std::io::Read;
use nf::FlowStateMsg;
use e2d2::utils::Flow;
use std::net::Ipv4Addr;
extern crate futures;
extern crate hyper;
extern crate tokio_core;
use std::io::{self, Write};
use self::futures::{Future, Stream};
use self::hyper::Client;
use self::tokio_core::reactor::Core;
use self::hyper::{Method, Request};
use self::hyper::header::{ContentLength, ContentType};
use std::str;

fn post(json: String) {
    let mut core = Core::new().unwrap();
    let client = Client::new(&core.handle());

    let uri = "http://129.94.5.44:8081/stats/flowentry/add".parse().unwrap();
    let mut req = Request::new(Method::Post, uri);
    req.headers_mut().set(ContentType::json());
    req.headers_mut().set(ContentLength(json.len() as u64));
    req.set_body(json);
    let post = client.request(req);
    if let Result::Err(err) = core.run(post){
        println!("Error posting through rest call: {}",  err);
    }
}


fn get_json_string(ts: String, src_ip: String, dst_ip: String,
proto: String, src_port: String, dst_port: String) -> String {
let data = r#"{
"dpid": 69,
"cookie": [TIMESTAMP],
"table_id": 0,
"hard_timeout": 0,
"idle_timeout":45,
"priority": 20000,
"match":
          {
                "in_port": 15,
                "eth_type": 2048,
                "ipv4_src": "[SRC_IP]",
                "ipv4_dst": "[DST_IP]",
                "ip_proto": [PROTO],
                "[TRANSPORT_SRC]": [SRC_PORT],
                "[TRANSPORT_DST]" : [DST_PORT]
            },
"actions":
   [
        {
            "type":"GROUP",
            "group_id": 149
        }
   ]
}"#;
    let mut data_string = String::from(data);
    data_string = data_string.replace("[TIMESTAMP]",&ts);
    data_string = data_string.replace("[SRC_IP]",&src_ip);
    data_string = data_string.replace("[DST_IP]",&dst_ip);
    data_string = data_string.replace("[PROTO]",&proto);

    if proto == "6"{
    data_string = data_string.replace("[TRANSPORT_SRC]","tcp_src");
    data_string = data_string.replace("[TRANSPORT_DST]","tcp_dst");
    } else if proto == "17" {
    data_string = data_string.replace("[TRANSPORT_SRC]","udp_src");
    data_string = data_string.replace("[TRANSPORT_DST]","udp_dst");
    } else {
    print!("Unexpected protocol");
    }

    data_string = data_string.replace("[SRC_PORT]",&src_port);
    data_string = data_string.replace("[DST_PORT]",&dst_port);
    data_string
}

pub fn send_to_controller( msg : FlowStateMsg ) {


  let rx_flow: Flow = msg.flow;



  let ip_src : String = format!("{:?}",Ipv4Addr::from(rx_flow.src_ip));
  let ip_dest : String = format!("{:?}",Ipv4Addr::from(rx_flow.dst_ip));
  let source_port : String = format!("{:?}",rx_flow.src_port);
  let dest_port : String  = format!("{:?}",rx_flow.dst_port);
  let protocol : String = format!("{:?}",rx_flow.proto);
  let now_time : String  = format!("{:?}",time::precise_time_ns());

  let answer:String = get_json_string(now_time, ip_src, ip_dest, protocol, source_port, dest_port);
  let answer2 = answer.clone();

  post(answer);
}
