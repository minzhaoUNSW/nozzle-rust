extern crate time;
use e2d2::utils::Flow;
use std::option::Option;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use fnv::FnvHasher;
use std::hash::BuildHasherDefault;
use nf::FlowStateMsg;

const DELETE_OLDER_THAN: u64 = 30000000000;
const LONG_FLOW_LENGTH: u32 = 4000000;
type FnvHash = BuildHasherDefault<FnvHasher>;

#[derive(Debug)]
struct FlowHashEntry {
   val : FlowStateMsg,
   entry_prev : Option<Flow>,
   entry_next : Option<Flow>,
   time_pkt: u64,
}

pub struct FlowHashTable {
  hash_map: HashMap<Flow, FlowHashEntry, FnvHash>,
  latest_entry: Option<Flow>,
  oldest_entry: Option<Flow>,
}

impl FlowHashTable {
  pub fn new() -> FlowHashTable {
    FlowHashTable {
      hash_map: HashMap::<Flow, FlowHashEntry, FnvHash>::with_hasher(Default::default()),
      latest_entry : Option::None,
      oldest_entry : Option::None,
    }
  }

  fn insert_latest(&mut self, msg: FlowStateMsg, now_time: u64) {
    let flow = msg.flow;
    let entry = FlowHashEntry {
      val: msg,
      time_pkt: now_time,
      entry_prev: Option::None,
      entry_next: self.latest_entry,
    };

    self.hash_map.insert(flow.clone(), entry);
    // update entry_prev of latest_entry.
    if let Option::Some(prev_flow) = self.latest_entry {
      let ref mut prev_entry_val = self.hash_map.get_mut(&prev_flow).expect("Error wild pointer during insert!");
      prev_entry_val.entry_prev = Option::Some(flow.clone());
    }
    self.latest_entry = Option::Some(flow.clone());
    if self.oldest_entry.is_none() {
      self.oldest_entry = Option::Some(flow.clone());
    }
  }

  fn delete_old_entries(&mut self, now_time: u64) {

    {
      let mut iter_flow = self.oldest_entry;
      while let Option::Some(oldest_flow) = iter_flow {
        match self.hash_map.entry(oldest_flow) {
          Entry::Occupied(oldest_entry) => {
            if oldest_entry.get().time_pkt < (now_time - DELETE_OLDER_THAN) {
              iter_flow = oldest_entry.get().entry_prev;
              oldest_entry.remove_entry();
            } else {
              self.oldest_entry = iter_flow;
              break;
            }
          }
          Entry::Vacant(_) => {
            panic!("Error wild pointer during delete_old_entries!");
          }
        }
      }
    }
    match self.oldest_entry {
      Option::Some(entry_flow) => {
        let ref mut entry = self.hash_map.get_mut(&entry_flow).expect("Wild pointer during delete_old_entries!");
        entry.entry_next = Option::None;
      }
      Option::None => {
        self.latest_entry = Option::None;
      }
    }
  }

  pub fn flow_state_new(&mut self, msg: FlowStateMsg) -> bool {
    let msg_copy = msg.clone();
    let now_time = time::precise_time_ns();
    let mut answer:bool  = false;

    if !self.hash_map.contains_key(&msg.flow){
       self.insert_latest(msg, now_time);
    } else {
       let mut h_up: Option<Flow> = Option::None;
       let mut  h_down: Option<Flow> = Option::None;
       {
       let ref mut entry_val = self.hash_map.get_mut(&msg.flow).expect("Error wild pointer during accesing in els!");

       // if the flow is already pushed, dont waste time in processing it and throw the packet away
       if entry_val.val.pushed == true {
         return false;
       }
       //update length and pushed state
       entry_val.val.len = (entry_val).val.len + msg.len;
       entry_val.time_pkt = now_time;
       if entry_val.val.len > LONG_FLOW_LENGTH {
         answer = true;
         entry_val.val.pushed = true;
       }

       if self.latest_entry == Some(msg.flow){
           return answer;
       };

       h_up = entry_val.entry_prev.clone();
       h_down = entry_val.entry_next.clone();

       //Update current entry
       entry_val.entry_next = self.latest_entry.clone();
       entry_val.entry_prev = Option::None;
       }

       //Update previous latest entry
       if let Option::Some(prev_flow) = self.latest_entry {
          let ref mut prev_entry_val = self.hash_map.get_mut(&prev_flow).expect("Error wild pointer during insert!");
          assert_eq!(prev_entry_val.entry_prev,None);
          prev_entry_val.entry_prev = Option::Some(msg.flow.clone());
       }

       if (self.oldest_entry == Option::Some(msg.flow)) && (self.latest_entry != Option::Some(msg.flow)){
           // Update oldest entry pointer since the current entry will become the latest_entry
           self.oldest_entry = h_up.clone();
       }


       //Set current flow as latest
       self.latest_entry = Option::Some(msg.flow.clone());

       //Update entry above
       if let Some(flow_up) = h_up {
           let ref mut entry_up_val = self.hash_map
                                   .get_mut(&flow_up)
                                   .expect("Error wild pointer during delete_entry!");
           entry_up_val.entry_next = h_down.clone();
       }

       //Update entry below
       if let Some(flow_down) = h_down{
           let ref mut entry_down_val = self.hash_map
                                   .get_mut(&flow_down)
                                   .expect("Error wild pointer during delete_entry!");
           entry_down_val.entry_prev = h_up.clone();
       }
    }

    self.delete_old_entries(now_time);
//    self.debug(&msg_copy.flow);
    answer
  }
}
