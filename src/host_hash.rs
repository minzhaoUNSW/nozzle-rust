extern crate time;
use e2d2::utils::Flow;
use std::option::Option;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use fnv::FnvHasher;
use std::hash::BuildHasherDefault;
use nf::HostFlowMsg;
use host_flow_hash::FlowPktMsg;
use host_flow_hash::FlowHashTable;

const DELETE_OLDER_THAN: u64 = 20000000000;
const POST_TO_NATS_INTERVAL : u64 = 10000000000; //in ns- 10^9
const LONG_FLOW_LENGTH: u32 = 4000000;
type FnvHash = BuildHasherDefault<FnvHasher>;

#[derive(Debug)]
pub struct HostConnInfo {
   pub host_ip : u32,
   pub num_of_tcp_pkt : u64,
   pub num_of_tcp_conn : u64,
   pub num_of_udp_pkt : u64,
   pub num_of_udp_conn : u64,
   pub num_of_dns_pkt : u64,
   pub tcp_volume : u32,
   pub udp_volume : u32,
}

#[derive(Debug)]
struct HostHashEntry {
   val : HostConnInfo,
   flow_table : FlowHashTable,
   entry_prev : Option<u32>,
   entry_next : Option<u32>,

   time_pkt: u64,
   time_last_post : u64,
}

pub struct HostHashTable {
  hash_map: HashMap<u32, HostHashEntry, FnvHash>,
  latest_entry: Option<u32>,
  oldest_entry: Option<u32>,
}

impl HostHashTable {
  pub fn new() -> HostHashTable {
    HostHashTable {
      hash_map: HashMap::<u32, HostHashEntry, FnvHash>::with_hasher(Default::default()),
      latest_entry : Option::None,
      oldest_entry : Option::None,
    }
  }


  fn insert_latest(&mut self, msg: HostFlowMsg, now_time: u64) {
    let proto = msg.flow.proto;
    let host_ip = msg.host_ip;
    let mut num_of_udp_pkt_tmp = 0;
    let mut num_of_tcp_pkt_tmp = 0;
    let mut flow_hash_table = FlowHashTable::new();
    let tmp_flow_state_msg = FlowPktMsg{
      flow : msg.flow,
      len : msg.udp_len+msg.tcp_len,
      potential_ele : false,
    };

    flow_hash_table.flow_state_new(tmp_flow_state_msg);

    match proto {
      6 => num_of_tcp_pkt_tmp+=1,
      17 => num_of_udp_pkt_tmp+=1,
      _ => {},
    }

    let first_conn_info = HostConnInfo{
      host_ip : host_ip,
      num_of_tcp_pkt : num_of_tcp_pkt_tmp,
      num_of_tcp_conn : num_of_tcp_pkt_tmp,// since it is the first packet of a new connection
      num_of_udp_pkt : num_of_udp_pkt_tmp,
      num_of_udp_conn : num_of_udp_pkt_tmp,
      num_of_dns_pkt : 0,
      tcp_volume : msg.tcp_len,
      udp_volume : msg.udp_len,
      //time_last_active : time::precise_time_ns(),
      //time_last_post : time::precise_time_ns(),
      //flow_table : flow_hash_table,
    };

    //println!("self-len,{},first conn , proto {:?}, host_ip {:?}, first_conn_info {:?}", self.hash_map.len(),proto, host_ip, first_conn_info);

    let entry = HostHashEntry {
      val: first_conn_info,
      time_pkt: now_time,
      time_last_post : now_time,
      flow_table : flow_hash_table,
      entry_prev: Option::None,
      entry_next: self.latest_entry,
    };

    self.hash_map.insert(host_ip.clone(), entry);
    // update entry_prev of latest_entry.
    if let Option::Some(prev_host) = self.latest_entry {
      let ref mut prev_entry_val = self.hash_map.get_mut(&prev_host).expect("Error wild pointer during insert!");
      prev_entry_val.entry_prev = Option::Some(host_ip.clone());
    }
    self.latest_entry = Option::Some(host_ip.clone());
    if self.oldest_entry.is_none() {
      self.oldest_entry = Option::Some(host_ip.clone());
    }
  }

  fn delete_old_entries(&mut self, now_time: u64) {

    {
      let mut iter_host = self.oldest_entry;
      while let Option::Some(oldest_host) = iter_host {
        match self.hash_map.entry(oldest_host) {
          Entry::Occupied(oldest_entry) => {
            if oldest_entry.get().time_pkt < (now_time - DELETE_OLDER_THAN) {
              iter_host = oldest_entry.get().entry_prev;
              oldest_entry.remove_entry();

              //println!("{:?}",now_time);
              //debug purpose, show if flow deletion happens

            } else {
              self.oldest_entry = iter_host;
              break;
            }
          }
          Entry::Vacant(_) => {
            panic!("Error wild pointer during delete_old_entries!");
          }
        }
      }
    }
    match self.oldest_entry {
      Option::Some(entry_host) => {
        let ref mut entry = self.hash_map.get_mut(&entry_host).expect("Wild pointer during delete_old_entries!");
        entry.entry_next = Option::None;
      }
      Option::None => {
        self.latest_entry = Option::None;
      }
    }
  }


  pub fn flow_state_new(&mut self, msg: HostFlowMsg) -> (bool, HostConnInfo, bool, Flow) {
    let msg_copy = msg.clone();
    let flow_result = msg.flow.clone();
    let now_time = time::precise_time_ns();
    let mut answer:bool  = false;
    let mut potential_ele_detected:bool = false;// for flow level elephant flow posing
    let mut host_info = HostConnInfo{
       host_ip : 0,
       num_of_tcp_pkt : 0,
       num_of_tcp_conn : 0,
       num_of_udp_pkt : 0,
       num_of_udp_conn : 0,
       num_of_dns_pkt : 0,
       tcp_volume : 0,
       udp_volume : 0,
    };



    if !self.hash_map.contains_key(&msg.host_ip){
       self.insert_latest(msg, now_time);
    } else {
       let mut h_up: Option<u32> = Option::None;
       let mut  h_down: Option<u32> = Option::None;
       //{
       // let total_len = self.hash_map.len();
       // println!("number of entry :{}",total_len);
       //}


       {
       let ref mut entry_val = self.hash_map.get_mut(&msg.host_ip).expect("Error wild pointer during accesing in els!");
       //let len = self.hash_map.clone().len();
       //update length and pushed state
       match msg.flow.proto {
        6 => entry_val.val.num_of_tcp_pkt+=1,
        17 => entry_val.val.num_of_udp_pkt+=1,
        _  => {},
       };

       let tmp_flow_state_msg = FlowPktMsg{
        flow : msg.flow,
        len : msg.tcp_len+msg.udp_len,
        potential_ele : false,//once start detection from 2nd pkt
       };

       let (potential_ele_detected, conn_type) = entry_val.flow_table.flow_state_new(tmp_flow_state_msg);

       match conn_type {
        1 => entry_val.val.num_of_tcp_conn += 1,
        2 => entry_val.val.num_of_udp_conn += 1,
        _ => {},
       };

       entry_val.val.tcp_volume += msg.tcp_len;
       entry_val.val.udp_volume += msg.udp_len;
       //entry_val.val.len = (entry_val).val.len + msg.len;
       entry_val.time_pkt = now_time;

       //now this host need to be posted to NATS
       if now_time - entry_val.time_last_post > POST_TO_NATS_INTERVAL{
        answer = true;
        entry_val.time_last_post = now_time;

        host_info = HostConnInfo{
          host_ip : entry_val.val.host_ip,
          num_of_tcp_pkt : entry_val.val.num_of_tcp_pkt,
          num_of_tcp_conn : entry_val.val.num_of_tcp_conn,
          num_of_udp_pkt : entry_val.val.num_of_udp_pkt,
          num_of_udp_conn : entry_val.val.num_of_udp_conn,
          num_of_dns_pkt : entry_val.val.num_of_dns_pkt,
          tcp_volume : entry_val.val.tcp_volume,
          udp_volume : entry_val.val.udp_volume,
        };

        entry_val.val.tcp_volume = 0; //reset volume for next posting interval
        entry_val.val.udp_volume = 0;
        entry_val.val.num_of_tcp_pkt = 0;
        entry_val.val.num_of_udp_pkt = 0;
        entry_val.val.num_of_dns_pkt = 0;

       };

       //println!("entry updated host volume:{:?}, num_of_tcp_pkt:{:?}, num_of_tcp_conn:{:?}",entry_val.val.volume,entry_val.val.num_of_tcp_pkt,entry_val.val.num_of_tcp_conn);
       //println!("******");

       if self.latest_entry == Some(msg.host_ip){
           return (answer,host_info, potential_ele_detected, flow_result);
       };

       h_up = entry_val.entry_prev.clone();
       h_down = entry_val.entry_next.clone();

       //Update current entry
       entry_val.entry_next = self.latest_entry.clone();
       entry_val.entry_prev = Option::None;
       }


       //Update previous latest entry
       if let Option::Some(prev_host) = self.latest_entry {
          let ref mut prev_entry_val = self.hash_map.get_mut(&prev_host).expect("Error wild pointer during insert!");
          assert_eq!(prev_entry_val.entry_prev,None);
          prev_entry_val.entry_prev = Option::Some(msg.host_ip.clone());
       }

       if (self.oldest_entry == Option::Some(msg.host_ip)) && (self.latest_entry != Option::Some(msg.host_ip)){
           // Update oldest entry pointer since the current entry will become the latest_entry
           self.oldest_entry = h_up.clone();
       }


       //Set current flow as latest
       self.latest_entry = Option::Some(msg.host_ip.clone());

       //Update entry above
       if let Some(host_up) = h_up {
           let ref mut entry_up_val = self.hash_map
                                   .get_mut(&host_up)
                                   .expect("Error wild pointer during delete_entry!");
           entry_up_val.entry_next = h_down.clone();
       }

       //Update entry below
       if let Some(host_down) = h_down{
           let ref mut entry_down_val = self.hash_map
                                   .get_mut(&host_down)
                                   .expect("Error wild pointer during delete_entry!");
           entry_down_val.entry_prev = h_up.clone();
       }
    }

    self.delete_old_entries(now_time);
//    self.debug(&msg_copy.flow);
    (answer, host_info, potential_ele_detected, flow_result)
  }
}
