#![feature(box_syntax)]
extern crate dns_parser;
extern crate e2d2;
extern crate fnv;
extern crate nats;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate time;

use dns_parser::Packet as Dns_Packet;
use dns_parser::RRData;
use dns_parser::Header as Dns_Header;

use self::nf::*;
use e2d2::config::{basic_opts, read_matches};
use e2d2::interface::*;
use e2d2::operators::*;
use e2d2::scheduler::*;
use std::env;
use std::error;
use std::fmt::Display;
use std::process;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};
use std::sync::Arc;
use std::thread;
use std::string::String;
use std::time::Instant;
use std::time::Duration;
use std::net::Ipv4Addr;
mod nf;
mod flow_hash;
mod host_hash;
mod host_flow_hash;
mod http_client;
use flow_hash::FlowHashTable;
use host_hash::HostConnInfo;
use host_hash::HostHashTable;
use http_client::send_to_controller;

const CONVERSION_FACTOR: f64 = 1000000000.;
const LONG_FLOW_LENGTH: u32 = 3500000;
const N_THREADS: u32 = 16;
const N_DNS_THREADS: u32 = 1;

const NATS_SERVER_URL: &'static str = "nats://nats-server:4222";
const DNS_INFO_NATS_SUBJECT: &'static str = "dns_info";
const HOST_INFO_NATS_SUBJECT: &'static str = "host_info";

fn test<T, S>(ports: Vec<T>, sched: &mut S)
where
    T: PacketRx + PacketTx + Display + Clone + 'static,
    S: Scheduler + Sized,
{
    println!("Receiving started");

    for port in &ports {
        println!("Port {}", port);
    }

    //let (rest_tx, rest_rx): (Sender<FlowStateMsg>, Receiver<FlowStateMsg>) = mpsc::channel();
    let (host_tx, host_rx): (Sender<HostConnInfo>, Receiver<HostConnInfo>) = mpsc::channel();
    let (dns_tx, dns_rx):(Sender<DnsMsg>, Receiver<DnsMsg>) = mpsc::channel();

    //let mut tx_cns = Vec::<Sender<FlowStateMsg>>::with_capacity(N_THREADS as usize);
    let mut host_tx_cns = Vec::<Sender<HostFlowMsg>>::with_capacity(N_THREADS as usize);
    let mut dns_cns = Vec::<Sender<DnsMsg>>::with_capacity(N_DNS_THREADS as usize);
    dns_cns.push(dns_tx);

    for id in 0..N_THREADS {
      println!("id:{:?}",id);

      //let (thread_tx, thread_rx): (Sender<FlowStateMsg>, Receiver<FlowStateMsg>) = mpsc::channel();
      let (host_thread_tx, host_thread_rx): (Sender<HostFlowMsg>, Receiver<HostFlowMsg>) = mpsc::channel();

      //let t_tx = rest_tx.clone();
      //tx_cns.push(thread_tx);

      let host_t_tx = host_tx.clone();
      host_tx_cns.push(host_thread_tx);

     /*  thread::spawn(move || {
        let mut hash_table = FlowHashTable::new();
        for msg in thread_rx {
           let msg_copy = msg.clone();
           let res = hash_table.flow_state_new(msg);
            if res == true  {
                  //println!("should post to flow info here");
                  if let Result::Err(err) = (t_tx).send(msg_copy) {
                   println!("Error sending through rest call: {}",  err);
                 }
             }
        }
       });*/

      thread::spawn(move || {
          let mut host_hash_table = HostHashTable::new();
          for host_msg in host_thread_rx {
             //let host_msg_copy = host_msg.clone();
             let (host_res,host_info) = host_hash_table.flow_state_new(host_msg);
             if host_res == true  {
                   if let Result::Err(err) = (host_t_tx).send(host_info) {
                     println!("Error sending through rest call: {}",  err);
                   }
               }
           }
      });

    }

    // Thread to call REST server with messages of length > 5MB.
   /* thread::spawn(move || {
        for msg in rest_rx {
	        //println!("q get");
            send_to_controller(msg);
        }
    });*/

    // Thread to process posted host info
    thread::spawn(move || {
        let mut host_nats_client = nats::Client::new(NATS_SERVER_URL).unwrap();
        for msg in host_rx {
            //println!("publishing to NATS, {:?}",);
            if let Err(_) = host_info_publish_to_nats_server(&mut host_nats_client, &msg) {
                println!("Error publishing to NATS server.");
            }
        }
    });

    // Thread spawn to call DNS processing functions
    thread::spawn(move || {
        let NATS_SERVER_PUSH_DELAY: Duration = Duration::new(1, 0);
        // Create the nats client for publishing data.
        let mut nats_client = nats::Client::new(NATS_SERVER_URL).unwrap();

        // Create data structures for maintaining state.
        let mut start = Instant::now();
//        let mut msg_store = DnsMsg {
//            data : Vec::new()
//        };
        let mut msg_store: Vec<String> = Vec::new();

        for msg in dns_rx {
            msg_store.push(get_dns_json_string(&msg)); 
         //   println!("continue or break");
       // }
       // println!("break");
    //    println!("MSG: {:?}",msg_store);
            // Only publish at approximately constant delays.
        let elapsed = start.elapsed();
         if (elapsed >= NATS_SERVER_PUSH_DELAY) {
            if let Err(_) = dns_info_publish_to_nats_server(&mut nats_client, &msg_store) {
               println!("Error publishing to NATS server.");
            }
              start = Instant::now();
              //  msg_store.data.clear();
             msg_store.clear();
         }
        }
    });

             //   &tx_cns,
              //  &host_tx_cns,
              //&dns_cns,
    let mut pipelines: Vec<_> = ports
        .iter()
        .map(|port| {
            ttl_nf(
                ReceiveBatch::new(port.clone()),
                sched,
                &host_tx_cns,
                &dns_cns,
            )
             .send(port.clone())
        }).collect();
    println!("Running {} pipelines", pipelines.len());

    if pipelines.len() > 1 {
        sched.add_task(merge(pipelines)).unwrap()
    } else {
        sched.add_task(pipelines.pop().unwrap()).unwrap()
    };
}

fn dns_info_publish_to_nats_server(nats_client: &mut nats::Client, dns_msg: &[String]) -> Result<(), Box<error::Error>> {
   // println!("{:?}\n\n",dns_msg);
    let encoded = serde_json::to_string(dns_msg)?;
    nats_client.publish(DNS_INFO_NATS_SUBJECT, encoded.as_bytes())?;
    Result::Ok(())

}

fn host_info_publish_to_nats_server(nats_client: &mut nats::Client, host_msg: &HostConnInfo) -> Result<(), Box<error::Error>> {
    let encoded = get_host_json_string(host_msg);
    nats_client.publish(HOST_INFO_NATS_SUBJECT, encoded.as_bytes())?;
    Result::Ok(())
}

fn get_dns_json_string(dns_msg: &DnsMsg) -> String {
let data = r#"{
"host_ip": [HOSTIP],
"publishing_time": [TIMESTAMP],
"id": [ID],
"QUERY": [QUERY],
"OPCODE": [OPCODE],
"NUM_QUESTIONS": [NUM_QUESTIONS],
"NUM_ANSWERS": [NUM_ANSWERS],
"NUM_NAME_SERVERS": [NUM_NAME_SERVERS],
"RESPONSE_CODE" : [RESPONSE_CODE],
}"#;
  let host_ip : String = format!("{:?}",Ipv4Addr::from(dns_msg.host_ip));
  let id: String = format!("{:?}",dns_msg.header.id);
  let query : String = format!("{:?}",dns_msg.header.query);
  let opcode : String  = format!("{:?}",dns_msg.header.opcode);
  let num_questions : String = format!("{:?}",dns_msg.header.questions);
  let num_answers : String = format!("{:?}",dns_msg.header.answers);
  let num_name_servers : String = format!("{:?}",dns_msg.header.nameservers);
  let now_time : String  = format!("{:?}",time::precise_time_ns());
  let response_code : String = format!("{:?}",dns_msg.header.response_code);

  let mut data_string = String::from(data);
  data_string = data_string.replace("[HOSTIP]",&host_ip);
  data_string = data_string.replace("[TIMESTAMP]",&now_time);
  data_string = data_string.replace("[ID]",&id);
  data_string = data_string.replace("[QUERY]",&query);
  data_string = data_string.replace("[OPCODE]",&opcode);
  data_string = data_string.replace("[NUM_QUESTIONS]",&num_questions);
  data_string = data_string.replace("[NUM_ANSWERS]",&num_answers);
  data_string = data_string.replace("[NUM_NAME_SERVERS]",&num_name_servers);
  data_string = data_string.replace("[RESPONSE_CODE]",&response_code);
  data_string
}

fn get_host_json_string(host_msg: &HostConnInfo) -> String {
let data = r#"{
"host_ip": [HOSTIP],
"publishing_time": [TIMESTAMP],
"num_of_tcp_conn": [NUM_OF_TCP_CONN],
"num_of_tcp_pkt": [NUM_OF_TCP_PKT],
"num_of_udp_conn": [NUM_OF_UDP_CONN],
"num_of_udp_pkt": [NUM_OF_UDP_PKT],
"num_of_dns_pkt": [NUM_OF_DNS_PKT],
"volume": [VOLUME]
}"#;

  let host_ip : String = format!("{:?}",Ipv4Addr::from(host_msg.host_ip));
  let num_of_tcp_conn: String = format!("{:?}",host_msg.num_of_tcp_conn);
  let num_of_tcp_pkt : String = format!("{:?}",host_msg.num_of_tcp_pkt);
  let num_of_udp_conn : String  = format!("{:?}",host_msg.num_of_udp_conn);
  let num_of_udp_pkt : String = format!("{:?}",host_msg.num_of_udp_pkt);
  let num_of_dns_pkt : String = format!("{:?}",host_msg.num_of_dns_pkt);
  let volume : String = format!("{:?}",host_msg.volume);
  let now_time : String  = format!("{:?}",time::precise_time_ns());

  let mut data_string = String::from(data);
  data_string = data_string.replace("[HOSTIP]",&host_ip);
  data_string = data_string.replace("[TIMESTAMP]",&now_time);
  data_string = data_string.replace("[NUM_OF_TCP_CONN]",&num_of_tcp_conn);
  data_string = data_string.replace("[NUM_OF_TCP_PKT]",&num_of_tcp_pkt);
  data_string = data_string.replace("[NUM_OF_UDP_CONN]",&num_of_udp_conn);
  data_string = data_string.replace("[NUM_OF_UDP_PKT]",&num_of_udp_pkt);
  data_string = data_string.replace("[NUM_OF_DNS_PKT]",&num_of_dns_pkt);
  data_string = data_string.replace("[VOLUME]",&volume);
  //println!("{:?}",data_string);
  data_string
}

fn main() {
    let opts = basic_opts();

    let args: Vec<String> = env::args().collect();
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => panic!(f.to_string()),
    };
    let configuration = read_matches(&matches, &opts);

    match initialize_system(&configuration) {
        Ok(mut context) => {
            context.start_schedulers();
            context.add_pipeline_to_run(Arc::new(move |p, s: &mut StandaloneScheduler| test(p, s)));
            context.execute();

            let mut pkts_so_far = (0, 0);
            let mut start = time::precise_time_ns() as f64 / CONVERSION_FACTOR;
            let sleep_time = Duration::from_millis(500);
            loop {
                thread::sleep(sleep_time); // Sleep for a bit
                let now = time::precise_time_ns() as f64 / CONVERSION_FACTOR;
                if now - start > 1.0 {
                    let mut rx = 0;
                    let mut tx = 0;
                    for port in context.ports.values() {
                        for q in 0..port.rxqs() {
                            let (rp, tp) = port.stats(q);
                            rx += rp;
                            tx += tp;
                        }
                    }
                    let pkts = (rx, tx);
                    println!(
                        "{:.2} OVERALL RX {:.2} TX {:.2}",
                        now - start,
                        (pkts.0 - pkts_so_far.0) as f64 / (now - start),
                        (pkts.1 - pkts_so_far.1) as f64 / (now - start)
                    );
                    start = now;
                    pkts_so_far = pkts;
                }
            }
        }
        Err(ref e) => {
            println!("Error: {}", e);
            if let Some(backtrace) = e.backtrace() {
                println!("Backtrace: {:?}", backtrace);
            }
            process::exit(1);
        }
    }
}
