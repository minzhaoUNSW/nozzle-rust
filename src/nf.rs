extern crate rand;
extern crate dns_parser;
use dns_parser::Packet as Dns_Packet;
use dns_parser::RRData;
use dns_parser::Header as Dns_Header;
use e2d2::headers::*;
use e2d2::operators::*;
use e2d2::scheduler::*;
use e2d2::utils::Flow;
use std::sync::mpsc::Sender;
use self::rand::Rng;

#[derive(Debug, Clone)]
pub struct HostFlowMsg {
   pub host_ip : u32,
   pub flow : Flow,
   pub udp_len : u32,
   pub tcp_len : u32
}

#[derive(Debug, Clone)]
pub struct FlowStateMsg {
   pub flow : Flow,
   pub len : u32,
   pub pushed : bool
}

#[derive(Debug,Clone)]
pub struct DnsMsg {
    pub host_ip : u32,
    pub server_ip : u32,
    pub header : Dns_Header,
    pub question_name : String,
    pub question_prefer_unicast : bool,
    pub question_qtype : String,
    pub question_qclass : String,
    pub answer_name : String,
    pub answer_data_type: String,
    pub answer_data_content: String,
    pub answer_ttl : String,
    pub answer_cls : String,
    pub name_server_name: String,
    pub name_server_type: String,
    pub name_server_content: String,
    pub name_server_ttl: String,
    pub name_server_cls: String,
    pub additional_name: String,
    pub additional_type: String,
    pub additional_content: String,
    pub additional_ttl: String,
    pub additional_cls: String,
 //   pub answer_data : u32,
}

const R_THREAD: u32 =  16;
const DNS_THREAD: u32 = 1;
/*tx: &Vec<Sender<FlowStateMsg>>, host_tx: &Vec<Sender<HostFlowMsg>>,, dns_tx: &Vec<Sender<DnsMsg>>*/
pub fn ttl_nf<T: 'static + Batch<Header = NullHeader>, S: Scheduler + Sized>(
    parent: T, _s: &mut S, host_tx: &Vec<Sender<HostFlowMsg>>, dns_tx: &Vec<Sender<DnsMsg>>) -> CompositionBatch {
    println!("Number of ttl_nf");
   // let tx_clone = tx.clone();
    //let dns_flag: bool = false;
    let dns_tx_clone = dns_tx.clone();
    let host_tx_clone = host_tx.clone();

    let mut groups = parent
        .parse::<MacHeader>()
        .parse::<IpHeader>()
        .group_by(2, box move |p| if {
            match p.get_header().flow() {
                Some(flow) => {

                    let mut result:bool = false;

                    //host level operation
                    let mut host_msg = HostFlowMsg {
                        host_ip: flow.dst_ip, //using src ip since the bug for ip resolving?
                        flow:flow,
                        udp_len : 0,
                        tcp_len : 0,
                    };

                    //len : p.get_header().length() as u32

                    if flow.proto == 17{
                      host_msg.udp_len = p.get_header().length() as u32;
                      if ((flow.dst_port == 53)||(flow.src_port ==53)) {
                        result = true;
                      }
                    } else {
                      if flow.proto == 6{
                        host_msg.tcp_len = p.get_header().length() as u32;
                      }
                    }
//call thread to send host data structure
                    let host_msg_clone = host_msg.clone();
                    let host_thread_index: u32 = host_msg.host_ip & (R_THREAD-1);
                    if let Result::Err(err) = host_tx_clone[host_thread_index as usize].send(host_msg){
                        println!("Error sending from pipeline for host: {}",err)
                    };

                    //println!("input host msg ip {}",host_msg_clone.host_ip);

//                    if flow.dst_port == 53 && flow.proto == 17 {
//                        true
//                    } else {false}
                    result
                },
                None => (false)
            }
            } {0} else {1}, _s,);

    let pipeline = groups
        .get_group(1)
        .unwrap()
        .map(box move |pkt| {
         /*   match pkt.get_header().flow() {
                Some(flow) => {
                    let flow_corrected = Flow {
                        src_ip: flow.src_ip,
                        dst_ip: flow.dst_ip,
                        src_port: flow.dst_port,
                        dst_port: flow.src_port,
                        proto: flow.proto,
                    };

                    let msg = FlowStateMsg {
                      flow: flow_corrected,
                      len: pkt.get_header().length() as u32,
                      pushed: false
                    };

                     let thread_index: u32  = msg.flow.src_ip & (R_THREAD-1);
                     if let Result::Err(err) = tx_clone[thread_index as usize].send(msg) {
                      println!("Error sending from pipeline: {}",  err);
                    }

                },
                None => ()
            };*/

        }).compose();

    let dns_pipeline = groups
        .get_group(0)
        .unwrap()
        .metadata(box move |pkt|{
            let flow = pkt.get_header().flow().unwrap();
            //let ip_size = pkt.get_header().length().unwrap(); 
            //pass pkt lengh indicated in ip header to next stage
            //(flow, ip_size)
            flow
        })
        .parse::<UdpHeader>()
        .map(box move |pkt| {
            let flow = pkt.read_metadata();
            //println!("FLOW:::{:?}",flow);
            match Dns_Packet::parse(pkt.get_payload()) {
                Ok(dns_pkt) => {
                    //println!("DNS Packet received in pipeline");
                    //println!("DNS:{:?}",dns_pkt);
               //     let mut dns_msg = DnsMsg {
                 //       data : Vec::new()
                   // };
                    //println!("{:?}",dns_pkt.header);
                    let mut dns_msg = DnsMsg{
                        host_ip : flow.dst_ip,
                        server_ip: flow.src_ip,
                        header : dns_pkt.header,
                        question_name : String::new(),
                        question_prefer_unicast : true,
                        question_qtype : String::new(),
                        question_qclass : String::new(),
                        answer_name : String::new(),
                        answer_data_type : String::new(),
                        answer_data_content : String::new(),
                        answer_ttl : String::new(),
                        answer_cls: String::new(),
                        name_server_name: String::new(),
                        name_server_type : String::new(),
                        name_server_content: String::new(),
                        name_server_ttl: String::new(),
                        name_server_cls: String::new(),
                        additional_name: String::new(),
                        additional_type: String::new(),
                        additional_content: String::new(),
                        additional_ttl: String::new(),
                        additional_cls: String::new(),
                        //append ttl values to string
                    };
                 for x in &dns_pkt.questions{
                        dns_msg.question_name.push_str(&x.qname.to_string());
                        dns_msg.question_prefer_unicast = x.prefer_unicast;
                        dns_msg.question_qtype.push_str(&format!("{:?}",x.qtype));
                        dns_msg.question_qclass.push_str(&format!("{:?}",x.qclass));

                        //dns_msg.question_name.push('|');
                      }

                  for z in &dns_pkt.nameservers{

                      dns_msg.name_server_name.push_str(&z.name.to_string());
                      dns_msg.name_server_name.push('|');
                      dns_msg.name_server_ttl.push_str(&z.ttl.to_string());
                      dns_msg.name_server_ttl.push('|');
                      dns_msg.name_server_cls.push_str(&format!("{:?}",z.cls));
                      dns_msg.name_server_cls.push('|');
                    //println!("{:?}",y);
                     // dns_msg.answer_data = y.data;
                      match z.data{
                        RRData::A(value) => {
                          dns_msg.name_server_type.push_str("A");
                          dns_msg.name_server_type.push('|');
                          dns_msg.name_server_content.push_str(&value.to_string());
                          dns_msg.name_server_content.push('|');
                        },
                        RRData::AAAA(value) =>{
                          dns_msg.name_server_type.push_str("AAAA");
                          dns_msg.name_server_type.push('|');
                          dns_msg.name_server_content.push_str(&value.to_string());
                          dns_msg.name_server_content.push('|');
                        },
                        RRData::CNAME(value) =>{
                          dns_msg.name_server_type.push_str("CNAME");
                          dns_msg.name_server_type.push('|');
                          dns_msg.name_server_content.push_str(&value.to_string());
                          dns_msg.name_server_content.push('|');
                        },
                        RRData::NS(value) =>{
                          dns_msg.name_server_type.push_str("NS");
                          dns_msg.name_server_type.push('|');
                          dns_msg.name_server_content.push_str(&value.to_string());
                          dns_msg.name_server_content.push('|');
                        },
                        RRData::TXT(ref value) =>{
                          dns_msg.name_server_type.push_str("TXT");
                          dns_msg.name_server_type.push('|');
                          dns_msg.name_server_content.push_str(&value.to_string());
                          dns_msg.name_server_content.push('|');
                        },
                        RRData::PTR(value)=>{
                          dns_msg.name_server_type.push_str("PTR");
                          dns_msg.name_server_type.push('|');
                          dns_msg.name_server_content.push_str(&value.to_string());
                          dns_msg.name_server_content.push('|');
                        },
                        RRData::MX{preference,exchange}=>{
                          dns_msg.name_server_type.push_str("MX");
                          dns_msg.name_server_type.push('|');
                          dns_msg.name_server_content.push_str(&preference.to_string());
                          dns_msg.name_server_content.push(':');
                          dns_msg.name_server_content.push_str(&exchange.to_string());
                          dns_msg.name_server_content.push('|');
                        },
                        RRData::SRV{priority,weight,port,target}=>{
                          dns_msg.name_server_type.push_str("SRV");
                          dns_msg.name_server_type.push('|');
                          dns_msg.name_server_content.push_str(&priority.to_string());
                          dns_msg.name_server_content.push(':');
                          dns_msg.name_server_content.push_str(&weight.to_string());
                          dns_msg.name_server_content.push(':');
                          dns_msg.name_server_content.push_str(&port.to_string());
                          dns_msg.name_server_content.push(':');
                          dns_msg.name_server_content.push_str(&target.to_string());
                          dns_msg.name_server_content.push('|');

                        },
                        //SRV, SOA Unknown and MX is not covered
                        _=>()
                      };
                    }


                  for zz in &dns_pkt.additional{
                      dns_msg.additional_name.push_str(&zz.name.to_string());
                      dns_msg.additional_name.push('|');
                      dns_msg.additional_ttl.push_str(&zz.ttl.to_string());
                      dns_msg.additional_ttl.push('|');
                      dns_msg.additional_cls.push_str(&format!("{:?}",zz.cls));
                      dns_msg.additional_cls.push('|');
                    //println!("{:?}",y);
                     // dns_msg.answer_data = y.data;
                      match zz.data{
                        RRData::A(value) => {
                          dns_msg.additional_type.push_str("A");
                          dns_msg.additional_type.push('|');
                          dns_msg.additional_content.push_str(&value.to_string());
                          dns_msg.additional_content.push('|');
                        },
                        RRData::AAAA(value) =>{
                          dns_msg.additional_type.push_str("AAAA");
                          dns_msg.additional_type.push('|');
                          dns_msg.additional_content.push_str(&value.to_string());
                          dns_msg.additional_content.push('|');
                        },
                        RRData::CNAME(value) =>{
                          dns_msg.additional_type.push_str("CNAME");
                          dns_msg.additional_type.push('|');
                          dns_msg.additional_content.push_str(&value.to_string());
                          dns_msg.additional_content.push('|');
                        },
                        RRData::NS(value) =>{
                          dns_msg.additional_type.push_str("NS");
                          dns_msg.additional_type.push('|');
                          dns_msg.additional_content.push_str(&value.to_string());
                          dns_msg.additional_content.push('|');
                        },
                        RRData::TXT(ref value) =>{
                          dns_msg.additional_type.push_str("TXT");
                          dns_msg.additional_type.push('|');
                          dns_msg.additional_content.push_str(&value.to_string());
                          dns_msg.additional_content.push('|');
                        },
                        RRData::PTR(value)=>{
                          dns_msg.additional_type.push_str("PTR");
                          dns_msg.additional_type.push('|');
                          dns_msg.additional_content.push_str(&value.to_string());
                          dns_msg.additional_content.push('|');
                        },
                        RRData::MX{preference,exchange}=>{
                          dns_msg.additional_type.push_str("MX");
                          dns_msg.additional_type.push('|');
                          dns_msg.additional_content.push_str(&preference.to_string());
                          dns_msg.additional_content.push(':');
                          dns_msg.additional_content.push_str(&exchange.to_string());
                          dns_msg.additional_content.push('|');
                        },
                        RRData::SRV{priority,weight,port,target}=>{
                          dns_msg.additional_type.push_str("SRV");
                          dns_msg.additional_type.push('|');
                          dns_msg.additional_content.push_str(&priority.to_string());
                          dns_msg.additional_content.push(':');
                          dns_msg.additional_content.push_str(&weight.to_string());
                          dns_msg.additional_content.push(':');
                          dns_msg.additional_content.push_str(&port.to_string());
                          dns_msg.additional_content.push(':');
                          dns_msg.additional_content.push_str(&target.to_string());
                          dns_msg.additional_content.push('|');

                        },
                        //SOA is not covered
                        _=>()
                      };
                    }



                  for y in &dns_pkt.answers{

                      dns_msg.answer_name.push_str(&y.name.to_string());
                      dns_msg.answer_name.push('|');
                      dns_msg.answer_ttl.push_str(&y.ttl.to_string());
                      dns_msg.answer_ttl.push('|');
                      dns_msg.answer_cls.push_str(&format!("{:?}",y.cls));
                      dns_msg.answer_cls.push('|');
                    //println!("{:?}",y);
                     // dns_msg.answer_data = y.data;
                      match y.data{
                        RRData::A(value) => {
                          dns_msg.answer_data_type.push_str("A");
                          dns_msg.answer_data_type.push('|');
                          dns_msg.answer_data_content.push_str(&value.to_string());
                          dns_msg.answer_data_content.push('|');
                        },
                        RRData::AAAA(value) =>{
                          dns_msg.answer_data_type.push_str("AAAA");
                          dns_msg.answer_data_type.push('|');
                          dns_msg.answer_data_content.push_str(&value.to_string());
                          dns_msg.answer_data_content.push('|');
                        },
                        RRData::CNAME(value) =>{
                          dns_msg.answer_data_type.push_str("CNAME");
                          dns_msg.answer_data_type.push('|');
                          dns_msg.answer_data_content.push_str(&value.to_string());
                          dns_msg.answer_data_content.push('|');
                        },
                        RRData::NS(value) =>{
                          dns_msg.answer_data_type.push_str("NS");
                          dns_msg.answer_data_type.push('|');
                          dns_msg.answer_data_content.push_str(&value.to_string());
                          dns_msg.answer_data_content.push('|');
                        },
                        RRData::TXT(ref value) =>{
                          dns_msg.answer_data_type.push_str("TXT");
                          dns_msg.answer_data_type.push('|');
                          dns_msg.answer_data_content.push_str(&value.to_string());
                          dns_msg.answer_data_content.push('|');
                        },
                        RRData::PTR(value)=>{
                          dns_msg.answer_data_type.push_str("PTR");
                          dns_msg.answer_data_type.push('|');
                          dns_msg.answer_data_content.push_str(&value.to_string());
                          dns_msg.answer_data_content.push('|');
                        },
                        RRData::MX{preference,exchange}=>{
                          dns_msg.answer_data_type.push_str("MX");
                          dns_msg.answer_data_type.push('|');
                          dns_msg.answer_data_content.push_str(&preference.to_string());
                          dns_msg.answer_data_content.push(':');
                          dns_msg.answer_data_content.push_str(&exchange.to_string());
                          dns_msg.answer_data_content.push('|');
                        },
                        RRData::SRV{priority,weight,port,target}=>{
                          dns_msg.answer_data_type.push_str("SRV");
                          dns_msg.answer_data_type.push('|');
                          dns_msg.answer_data_content.push_str(&priority.to_string());
                          dns_msg.answer_data_content.push(':');
                          dns_msg.answer_data_content.push_str(&weight.to_string());
                          dns_msg.answer_data_content.push(':');
                          dns_msg.answer_data_content.push_str(&port.to_string());
                          dns_msg.answer_data_content.push(':');
                          dns_msg.answer_data_content.push_str(&target.to_string());
                          dns_msg.answer_data_content.push('|');

                        },
                        //SRV, SOA Unknown and MX is not covered
                        _=>()
                      };
                    }

                      //expect to have only one question here
                 //     for x in &dns_pkt.questions{
                   //     dns_msg.question_name.push_str(&x.qname.to_string());
                    //    dns_msg.question_name.push('|');
                     // }
                      //let y_clone = y.clone();

                    let dns_msg_clone = dns_msg.clone();
                    
                    let dns_thread_index: u32  = DNS_THREAD-1;
                    if let Result::Err(err) = dns_tx_clone[dns_thread_index as usize].send(dns_msg_clone) {
                          println!("Error sending from pipeline: {}",  err);
                    }
                },
                Err(error) => {
                    //println!("DNS Parsing failed {}", error);
                }
            };
        }).compose();

     merge(vec![pipeline, dns_pipeline]).compose()
}
