#![feature(box_syntax)]
extern crate dns_parser;
extern crate e2d2;
extern crate fnv;
extern crate nats;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate time;

use dns_parser::Packet as Dns_Packet;
use dns_parser::RRData;
use dns_parser::Header as Dns_Header;

use self::nf::*;
use e2d2::utils::Flow;
use e2d2::config::{basic_opts, read_matches};
use e2d2::interface::*;
use e2d2::operators::*;
use e2d2::scheduler::*;
use std::env;
use std::error;
use std::fmt::Display;
use std::process;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};
use std::sync::Arc;
use std::thread;
use std::string::String;
use std::time::Instant;
use std::time::Duration;
use std::net::Ipv4Addr;
mod nf;
mod flow_hash;
mod host_hash;
mod host_flow_hash;
mod http_client;
use flow_hash::FlowHashTable;
use host_hash::HostConnInfo;
use host_hash::HostHashTable;
use http_client::send_to_controller;

const CONVERSION_FACTOR: f64 = 1000000000.;
const LONG_FLOW_LENGTH: u32 = 3500000;
const N_THREADS: u32 = 16;
const N_DNS_THREADS: u32 = 1;

const NATS_SERVER_URL: &'static str = "nats://nats-server:4222";
const DNS_INFO_NATS_SUBJECT: &'static str = "dns_info";
const HOST_INFO_NATS_SUBJECT: &'static str = "host_info";
const FLOW_INFO_NATS_SUBJECT: &'static str = "flow_info";

fn test<T, S>(ports: Vec<T>, sched: &mut S)
where
    T: PacketRx + PacketTx + Display + Clone + 'static,
    S: Scheduler + Sized,
{
    println!("Receiving started");

    for port in &ports {
        println!("Port {}", port);
    }

    let (flow_tx, flow_rx): (Sender<Flow>, Receiver<Flow>) = mpsc::channel();
    let (host_tx, host_rx): (Sender<HostConnInfo>, Receiver<HostConnInfo>) = mpsc::channel();
    let (dns_tx, dns_rx):(Sender<DnsMsg>, Receiver<DnsMsg>) = mpsc::channel();

    let mut flow_tx_cns = Vec::<Sender<Flow>>::with_capacity(N_THREADS as usize);
    let mut host_tx_cns = Vec::<Sender<HostFlowMsg>>::with_capacity(N_THREADS as usize);
    let mut dns_cns = Vec::<Sender<DnsMsg>>::with_capacity(N_DNS_THREADS as usize);
    dns_cns.push(dns_tx);

    for id in 0..N_THREADS {
      println!("id:{:?}",id);

      let (flow_thread_tx, flow_thread_rx): (Sender<Flow>, Receiver<Flow>) = mpsc::channel();
      let (host_thread_tx, host_thread_rx): (Sender<HostFlowMsg>, Receiver<HostFlowMsg>) = mpsc::channel();

      let flow_t_tx = flow_tx.clone();
      flow_tx_cns.push(flow_thread_tx);

      let host_t_tx = host_tx.clone();
      host_tx_cns.push(host_thread_tx);

      thread::spawn(move || {
          let mut host_hash_table = HostHashTable::new();
          for host_msg in host_thread_rx {
             //let host_msg_copy = host_msg.clone();
             let (host_res,host_info,flow_res,flow_info) = host_hash_table.flow_state_new(host_msg);
             if host_res == true  {
                   if let Result::Err(err) = (host_t_tx).send(host_info) {
                     println!("Error sending through rest call: {}",  err);
                   }
               }

             if flow_res == true {
                  if let Result::Err(err) = (flow_t_tx).send(flow_info){
                    println!("Error sending through rest call: {}", err);
                  }
             }
           }
      });

    }

    // Thread to process elephant flow info
    thread::spawn(move || {

          let NATS_SERVER_PUSH_DELAY: Duration = Duration::new(1, 0);
          let mut flow_nats_client = nats::Client::new(NATS_SERVER_URL).unwrap();

        for msg in flow_rx {
          //println!("{:?}",msg);
          if let Err(_) = flow_info_publish_to_nats_server(&mut flow_nats_client, &get_flow_json_string(&msg)) {
                  println!("Error publishing to NATS server - flow");
            }

	        //println!("q get");
            //send_to_controller(msg);
        }
    });

    // Thread to process posted host info
    thread::spawn(move || {

        let NATS_SERVER_PUSH_DELAY: Duration = Duration::new(1, 0);
        let mut host_nats_client = nats::Client::new(NATS_SERVER_URL).unwrap();
        let mut start = Instant::now();
        let mut msg_store: Vec<String> = Vec::new();

        for msg in host_rx {
            msg_store.push(get_host_json_string(&msg));

            let elapsed = start.elapsed();
             if(elapsed >= NATS_SERVER_PUSH_DELAY){
                if let Err(_) = host_info_publish_to_nats_server(&mut host_nats_client, &msg_store) {
                  println!("Error publishing to NATS server.");
            }

              start = Instant::now();
              msg_store.clear();
             }

        }
    });

    // Thread spawn to call DNS processing functions
    thread::spawn(move || {
        let NATS_SERVER_PUSH_DELAY: Duration = Duration::new(1, 0);
        // Create the nats client for publishing data.
        let mut nats_client = nats::Client::new(NATS_SERVER_URL).unwrap();
        // Create data structures for maintaining state.
        let mut start = Instant::now();
        let mut msg_store: Vec<String> = Vec::new();

        for msg in dns_rx {

            msg_store.push(get_dns_json_string(&msg)); 
         //   println!("continue or break");
       // }
       // println!("break");
        //println!("MSG: {:?}",msg);
            // Only publish at approximately constant delays.
        let elapsed = start.elapsed();
         if (elapsed >= NATS_SERVER_PUSH_DELAY) {
            if let Err(_) = dns_info_publish_to_nats_server(&mut nats_client, &msg_store) {
               println!("Error publishing to NATS server.");
            }
              start = Instant::now();
              //  msg_store.data.clear();
             msg_store.clear();
         }
        }
    });

             //   &tx_cns,
              //  &host_tx_cns,
              //&dns_cns,
    let mut pipelines: Vec<_> = ports
        .iter()
        .map(|port| {
            ttl_nf(
                ReceiveBatch::new(port.clone()),
                sched,
                &host_tx_cns,
                &dns_cns,
            )
             .send(port.clone())
        }).collect();
    println!("Running {} pipelines", pipelines.len());

    if pipelines.len() > 1 {
        sched.add_task(merge(pipelines)).unwrap()
    } else {
        sched.add_task(pipelines.pop().unwrap()).unwrap()
    };
}

fn dns_info_publish_to_nats_server(nats_client: &mut nats::Client, dns_msg: &[String]) -> Result<(), Box<error::Error>> {
   // println!("{:?}\n\n",dns_msg);
    let encoded = serde_json::to_string(dns_msg)?;
    nats_client.publish(DNS_INFO_NATS_SUBJECT, encoded.as_bytes())?;
    Result::Ok(())

}

//host_msg: &HostConnInfo
fn host_info_publish_to_nats_server(nats_client: &mut nats::Client, host_msg: &[String]) -> Result<(), Box<error::Error>> {
    //let encoded = get_host_json_string(host_msg);
    let encoded = serde_json::to_string(host_msg)?;
    nats_client.publish(HOST_INFO_NATS_SUBJECT, encoded.as_bytes())?;
    Result::Ok(())
}

fn flow_info_publish_to_nats_server(nats_client: &mut nats::Client, flow_msg: &String) -> Result<(), Box<error::Error>> {
    //let encoded = get_host_json_string(host_msg);
    let encoded = serde_json::to_string(flow_msg)?;
    nats_client.publish(FLOW_INFO_NATS_SUBJECT, encoded.as_bytes())?;
    Result::Ok(())
}

//host ip, publishing_time, id, query_name, opcode, num_questions, num_answers, num_name_servers, response_code
fn get_dns_json_string(dns_msg: &DnsMsg) -> String {
let data = "[HOSTIP],[TIMESTAMP],[SERVERIP],[ID],[QUERY],[OPCODE],[AUTHORITATIVE],[TRUNCATED],[RECURSION_DESIRED],[RECURSION_AVAILABLE],[AUTHENTICATED_DATA],[CHECKING_DISABLED],[NUM_QUESTIONS],[NUM_ANSWERS],[NUM_NAME_SERVERS],[NUM_ADDITIONAL],[RESPONSE_CODE],[QUESTION_NAME],[QUESTION_PREFER_UNICAST],[QUESTION_QTYPE],[QUESTION_QCLASS],[ANSWER_NAME],[ANSWER_DATA_TYPE],[ANSWER_DATA_CONTENT],[ANSWER_TTL],[ANSWER_CLS],[NAME_SERVER_NAME],[NAME_SERVER_TYPE],[NAME_SERVER_CONTENT],[NAME_SERVER_TTL],[NAME_SERVER_CLS],[ADDITIONAL_NAME],[ADDITIONAL_TYPE],[ADDITIONAL_CONTENT],[ADDITIONAL_TTL],[ADDITIONAL_CLS]end_of_record";
  let host_ip : String = format!("{:?}",Ipv4Addr::from(dns_msg.host_ip));
  let server_ip : String = format!("{:?}",Ipv4Addr::from(dns_msg.server_ip));
  let id: String = format!("{:?}",dns_msg.header.id);
  let query : String = format!("{:?}",dns_msg.header.query);
  let opcode : String  = format!("{:?}",dns_msg.header.opcode);
  let authoritative : String = format!("{:?}",dns_msg.header.authoritative);
  let truncated : String = format!("{:?}",dns_msg.header.truncated);
  let recursion_desired : String = format!("{:?}",dns_msg.header.recursion_desired);
  let recursion_available : String = format!("{:?}",dns_msg.header.recursion_available);
  let authenticated_data : String = format!("{:?}",dns_msg.header.authenticated_data);
  let checking_disabled : String = format!("{:?}",dns_msg.header.checking_disabled);
  let num_questions : String = format!("{:?}",dns_msg.header.questions);
  let num_answers : String = format!("{:?}",dns_msg.header.answers);
  let num_name_servers : String = format!("{:?}",dns_msg.header.nameservers);
  let num_additional : String = format!("{:?}",dns_msg.header.additional);
  let now_time : String  = format!("{:?}",time::precise_time_ns());
  let response_code : String = format!("{:?}",dns_msg.header.response_code);
  let question_name : String = format!("{:?}",dns_msg.question_name);
  let question_prefer_unicast : String = format!("{:?}",dns_msg.question_prefer_unicast);
  let question_qtype : String = format!("{:?}",dns_msg.question_qtype);
  let question_qclass : String = format!("{:?}",dns_msg.question_qclass);
  let answer_name : String = format!("{:?}",dns_msg.answer_name);
  let answer_data_type : String = format!("{:?}",dns_msg.answer_data_type);
  let answer_data_content : String = format!("{:?}",dns_msg.answer_data_content);
  let answer_ttl : String = format!("{:?}",dns_msg.answer_ttl);
  let answer_cls : String = format!("{:?}",dns_msg.answer_cls);
  let name_server_name : String = format!("{:?}",dns_msg.name_server_name);
  let name_server_type : String = format!("{:?}",dns_msg.name_server_type);
  let name_server_content : String = format!("{:?}",dns_msg.name_server_content);
  let name_server_ttl : String = format!("{:?}",dns_msg.name_server_ttl);
  let name_server_cls : String = format!("{:?}",dns_msg.name_server_cls);
  let additional_name : String = format!("{:?}",dns_msg.additional_name);
  let additional_type : String = format!("{:?}",dns_msg.additional_type);
  let additional_content : String = format!("{:?}",dns_msg.additional_content);
  let additional_ttl : String = format!("{:?}",dns_msg.additional_ttl);
  let additional_cls : String = format!("{:?}",dns_msg.additional_cls);
 // let answer_data : String = format!("{:?}",dns_msg.answer_data);

  let mut data_string = String::from(data);
  data_string = data_string.replace("[HOSTIP]",&host_ip);
  data_string = data_string.replace("[SERVERIP]",&server_ip);
  data_string = data_string.replace("[TIMESTAMP]",&now_time);
  data_string = data_string.replace("[ID]",&id);
  data_string = data_string.replace("[QUERY]",&query);
  data_string = data_string.replace("[OPCODE]",&opcode);
  data_string = data_string.replace("[AUTHORITATIVE]",&authoritative);
  data_string = data_string.replace("[TRUNCATED]",&truncated);
  data_string = data_string.replace("[RECURSION_DESIRED]",&recursion_desired);
  data_string = data_string.replace("[RECURSION_AVAILABLE]",&recursion_available);
  data_string = data_string.replace("[AUTHENTICATED_DATA]",&authenticated_data);
  data_string = data_string.replace("[CHECKING_DISABLED]",&checking_disabled);
  data_string = data_string.replace("[NUM_QUESTIONS]",&num_questions);
  data_string = data_string.replace("[NUM_ANSWERS]",&num_answers);
  data_string = data_string.replace("[NUM_NAME_SERVERS]",&num_name_servers);
  data_string = data_string.replace("[NUM_ADDITIONAL]",&num_additional);
  data_string = data_string.replace("[RESPONSE_CODE]",&response_code);
  data_string = data_string.replace("[QUESTION_NAME]",&question_name);
  data_string = data_string.replace("[QUESTION_PREFER_UNICAST]",&question_prefer_unicast);
  data_string = data_string.replace("[QUESTION_QTYPE]",&question_qtype);
  data_string = data_string.replace("[QUESTION_QCLASS]",&question_qclass);
  data_string = data_string.replace("[ANSWER_NAME]",&answer_name);
  data_string = data_string.replace("[ANSWER_DATA_TYPE]",&answer_data_type);
  data_string = data_string.replace("[ANSWER_DATA_CONTENT]",&answer_data_content);
  data_string = data_string.replace("[ANSWER_TTL]",&answer_ttl);
  data_string = data_string.replace("[ANSWER_CLS]",&answer_cls);
  data_string = data_string.replace("[NAME_SERVER_NAME]",&name_server_name);
  data_string = data_string.replace("[NAME_SERVER_TYPE]",&name_server_type);
  data_string = data_string.replace("[NAME_SERVER_CONTENT]",&name_server_content);
  data_string = data_string.replace("[NAME_SERVER_TTL]",&name_server_ttl);
  data_string = data_string.replace("[NAME_SERVER_CLS]",&name_server_cls);
  data_string = data_string.replace("[ADDITIONAL_NAME]",&additional_name);
  data_string = data_string.replace("[ADDITIONAL_TYPE]",&additional_type);
  data_string = data_string.replace("[ADDITIONAL_CONTENT]",&additional_content);
  data_string = data_string.replace("[ADDITIONAL_TTL]",&additional_ttl);
  data_string = data_string.replace("[ADDITIONAL_CLS]",&additional_cls);

  data_string
}

//hostip,publishingtime,num of tcp conn, num of tcp pkt, num of udp conn, num of udp pkt, volume
fn get_host_json_string(host_msg: &HostConnInfo) -> String {
let data = "[HOSTIP],[TIMESTAMP],[NUM_OF_TCP_CONN],[NUM_OF_TCP_PKT],[NUM_OF_UDP_CONN],[NUM_OF_UDP_PKT],[TCP_VOLUME],[UDP_VOLUME]end_of_record";

  let host_ip : String = format!("{:?}",Ipv4Addr::from(host_msg.host_ip));
  let num_of_tcp_conn: String = format!("{:?}",host_msg.num_of_tcp_conn);
  let num_of_tcp_pkt : String = format!("{:?}",host_msg.num_of_tcp_pkt);
  let num_of_udp_conn : String  = format!("{:?}",host_msg.num_of_udp_conn);
  let num_of_udp_pkt : String = format!("{:?}",host_msg.num_of_udp_pkt);
//  let num_of_dns_pkt : String = format!("{:?}",host_msg.num_of_dns_pkt);
  let tcp_volume : String = format!("{:?}",host_msg.tcp_volume);
  let udp_volume : String = format!("{:?}",host_msg.udp_volume);
  let now_time : String  = format!("{:?}",time::precise_time_ns());

  let mut data_string = String::from(data);
  data_string = data_string.replace("[HOSTIP]",&host_ip);
  data_string = data_string.replace("[TIMESTAMP]",&now_time);
  data_string = data_string.replace("[NUM_OF_TCP_CONN]",&num_of_tcp_conn);
  data_string = data_string.replace("[NUM_OF_TCP_PKT]",&num_of_tcp_pkt);
  data_string = data_string.replace("[NUM_OF_UDP_CONN]",&num_of_udp_conn);
  data_string = data_string.replace("[NUM_OF_UDP_PKT]",&num_of_udp_pkt);
 // data_string = data_string.replace("[NUM_OF_DNS_PKT]",&num_of_dns_pkt);
  data_string = data_string.replace("[TCP_VOLUME]",&tcp_volume);
  data_string = data_string.replace("[UDP_VOLUME]",&udp_volume);
  //println!("{:?}",data_string);
  data_string
}

fn get_flow_json_string(flow_msg: &Flow) -> String {
let data = "[HOSTIP],[TIMESTAMP],[SRCIP],[HOSTPORT],[SRCPORT],[PROTO]end_of_record";

  let host_ip : String = format!("{:?}",Ipv4Addr::from(flow_msg.dst_ip));
  let src_ip: String = format!("{:?}",Ipv4Addr::from(flow_msg.src_ip));
  let src_port : String = format!("{:?}",flow_msg.dst_port);
  let dst_port : String  = format!("{:?}",flow_msg.src_port);
  let proto : String = format!("{:?}",flow_msg.proto);
//  let num_of_dns_pkt : String = format!("{:?}",host_msg.num_of_dns_pkt);
 // let volume : String = format!("{:?}",host_msg.volume);
  let now_time : String  = format!("{:?}",time::precise_time_ns());

  let mut data_string = String::from(data);
  data_string = data_string.replace("[HOSTIP]",&host_ip);
  data_string = data_string.replace("[TIMESTAMP]",&now_time);
  data_string = data_string.replace("[SRCIP]",&src_ip);
  data_string = data_string.replace("[HOSTPORT]",&src_port);
  data_string = data_string.replace("[SRCPORT]",&dst_port);
  data_string = data_string.replace("[PROTO]",&proto);
 // data_string = data_string.replace("[NUM_OF_DNS_PKT]",&num_of_dns_pkt);
  //println!("{:?}",data_string);
  data_string

}


fn main() {
    let opts = basic_opts();

    let args: Vec<String> = env::args().collect();
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => panic!(f.to_string()),
    };
    let configuration = read_matches(&matches, &opts);

    match initialize_system(&configuration) {
        Ok(mut context) => {
            context.start_schedulers();
            context.add_pipeline_to_run(Arc::new(move |p, s: &mut StandaloneScheduler| test(p, s)));
            context.execute();

            let mut pkts_so_far = (0, 0);
            let mut start = time::precise_time_ns() as f64 / CONVERSION_FACTOR;
            let sleep_time = Duration::from_millis(500);
            loop {
                thread::sleep(sleep_time); // Sleep for a bit
                let now = time::precise_time_ns() as f64 / CONVERSION_FACTOR;
                if now - start > 1.0 {
                    let mut rx = 0;
                    let mut tx = 0;
                    for port in context.ports.values() {
                        for q in 0..port.rxqs() {
                            let (rp, tp) = port.stats(q);
                            rx += rp;
                            tx += tp;
                        }
                    }
                    let pkts = (rx, tx);
                    println!(
                        "{:.2} OVERALL RX {:.2} TX {:.2}",
                        now - start,
                        (pkts.0 - pkts_so_far.0) as f64 / (now - start),
                        (pkts.1 - pkts_so_far.1) as f64 / (now - start)
                    );
                    start = now;
                    pkts_so_far = pkts;
                }
            }
        }
        Err(ref e) => {
            println!("Error: {}", e);
            if let Some(backtrace) = e.backtrace() {
                println!("Backtrace: {:?}", backtrace);
            }
            process::exit(1);
        }
    }
}
